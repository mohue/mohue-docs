# Setting up the API server

## Requirements
* NodeJS
* MongoDB

## Setup
Clone the code from the WebApp Submodule
Run the server with the following command:
`npm start`

The server should show a message like:
```
> nodemon server.js

[nodemon] 1.18.4
[nodemon] to restart at any time, enter "rs"
[nodemon] watching: *.*
[nodemon] starting "node server.js"
MOHUE API server started on: 3000
```

You can now send GET/POST request to the API to test if the server is working correctly.
