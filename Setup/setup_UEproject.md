# Setup

## Requirements
* Unreal Engine SDK
  * v4.20.x
* Installation of MOH:Allied Assault, Spearhead and Breakthrough.
* Gitlab account(https://dev.x-null.net) and access to the repository with it submodules
* git

## Installation
### Setup SSH keys
Create SSH keys and add it to Gitlab.
More detailed information can be found here: https://dev.x-null.net/help/ssh/README

### Clone repository
Clone the repository via the following commands. (Make sure that your SSH keys are enabled via Pageant or any other application):

`git clone --recursive -j8 git@dev.x-null.net:mohue4/mohue.git
cd mohue
git submodule update --init --recursive`

### Generate visual studio files
Right click on `mohue.project` and select `Generate Visual Studio project Files`.

### Refer to your MOHAA game directory
When launching the project for the first time, you will be asked to refer to your MOHAA directory. Make sure to set the correct path.

### Loading a map for the first time
When launching UE, a default template map is shown in the viewport. Delete everything in the world. After that drag the map into the viewport. It will take some time to load all assets into the engine.
