# Documentation
Here we store our documentation, this can be anything from setting up and installing the project to handling with errors.

## Overview

### Setup
* [Setting up the UE project](Setup/setup_UEproject.md)


### Unreal Engine
#### Errors
* [Lightning errors](Unreal_Engine/Errors/lightning_build_errors.md)

## Web app
* [Setting up the API server](WebApp/setup_APIserver.md)
* [Using the management web application](WebApp/using_managementmodule.md)
