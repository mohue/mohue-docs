# Lightning Build errors
## Overview
* [1. Map is not imported into an empty level]()

## 1. Map is not imported into an empty level
The map is not imported into an empty level. Make sure to delete the "default example level" first and then import a map.
